#!/usr/bin/env bash

set -e

if [ -z "$1" ]; then
  echo "Please provide the backup directory as argument"
  exit 1
fi

if [ ! -d "$1" ]; then
  echo "'$1' not found"
  exit 1
fi

cd $1

if [ ! -f "./nextcloud.tar" ] || [ ! -f "./postgres.tar" ]; then
  echo "Expected nextcloud.tar and postgres.tar in '$1'"
  exit 1
fi

assert_volume_inexistent () {
  docker volume ls -q | grep -w "$1" > /dev/null

  if [ $? -eq 0 ]; then
    echo "Volume '$1' already exists"
    exit 1
  fi
}

assert_volume_inexistent "nextcloud_db"
assert_volume_inexistent "nextcloud_nextcloud"

docker run --rm -v nextcloud_db:/recover -v $(pwd):/backup alpine sh -c "cd /recover && tar xvf /backup/postgres.tar"
docker run --rm -v nextcloud_nextcloud:/recover -v $(pwd):/backup alpine sh -c "cd /recover && tar xvf /backup/nextcloud.tar"
