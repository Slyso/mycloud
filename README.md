# Introduction

This is my personal cloud setup based on the dockerized version of Nextcloud. PostgreSQL is used as Nextcloud's database and Nginx as reverse proxy to enable a secure connection.

This setup should be portable to pretty much any environment. Personally, I am running this setup on a Raspberry Pi 3+. To make the cloud available from everywhere I enabled port forwarding on my router and obtained a domain name using FreeDNS. It is therefore important to use an encrypted connection, which was achieved by using Nginx and Certbot from Let's encrypt.

For professional use you'd want to use a hosting service and acquire your own domain but for private use the employment of FreeDNS and port forwarding are suitable.

Personally, I think this is a much better alternative to proprietary cloud solutions. Some advantages include:

- No web hosting required
- Free software components (free as in freedom)
- Control over everything
- No expenses (ignoring the Raspberry Pi's tiny power consumption)

# Domain name

You don't want to use Nextcloud over the bare IP address of your router because most web providers use dynamic addresses and so your IP changes every now and then. Additionally, you can only get your certificate signed by an authority if you have a domain name. When using the IP address to connect to Nextcloud you would need to sign the certificates by yourself. When doing that most browsers will warn you about an insecure connection. But some tools and browsers will even refuse to connect to the site.

To mitigate both problems there are FreeDNS services. Most notably https://freedns.afraid.org/

You can obtain your own domain name for free! The restriction is that your domain has to be a sub-domain of the domains owned by FreeDNS.

The "Dynamic DNS" section on the FreeDNS website allows us to fix the issue of the dynamic IP of our internet gateway. FreeDNS will provide you with a cron job to make this possible. When adding this job to your port forwarded device it will ping FreeDNS every few minutes to detect an IP change and to update the IP in the DNS entry if a change was detected.

You should now have your own domain (though, technically it's not quite your own) which will always reach your device in your home network which will run Nextcloud inside of Docker.


# Usage

Create a .env file containing the previously obtained domain name. The .env file contains environment variable definitions used by Docker.

```bash
echo "SERVER_NAME=example.com" > .env
```

Then choose two unused ports of your device as HTTP and HTTPS port and write them into the .env file. Normally you can use the standard ports 80 and 443. But I used port 8080 as HTTP port because I already have another web server running on my device.

```bash
echo "LOCAL_HTTP_PORT=80" >> .env
echo "LOCAL_HTTPS_PORT=443" >> .env
```

Then enable port forwarding on your router. If your device has the IP 1.2.3.4 in your local network you want to forward the public port 80 of your router to 1.2.3.4 on the port specified in LOCAL_HTTP_PORT. The router's public port 443 should forward to 1.2.3.4 on the port defined in LOCAL_HTTPS_PORT.

Then to run the Docker setup:

```bash
docker-compose build --pull
docker-compose up -d
```

After a moment you should be able to reach the Nextcloud welcome screen via your host name. But your connection is not encrypted yet. To fix that simply run the certbot script:

```bash
./certbot.sh
```

After that when reloading the web page you should automatically use a secure connection. Finally, set up the Nextcloud admin account. The connection will time out and if you reload the page it might look like the admin account wasn't created but if reloading after a few minutes the instance should be set up.


# Final steps

## Update configuration

To be able to use the smartphone apps with Nextcloud we need to modify the config file:

```bash
docker-compose exec nextcloud /bin/bash
vim config/config.php
```

Then we remove the following line:

```
'overwrite.cli.url' => 'http://nextcloud'
```

And add these lines:

```
'overwritehost' => 'example.com',
'overwriteprotocol' => 'https',
```

Where example.com is your host name.

## Database indexing

In your Nextcloud settings overview page you might see a message about missing database indexes. These provided commands have to be run inside the Nextcloud container as "www-data" user:

```bash
docker-compose exec nextcloud /bin/bash
sudo -u www-data PHP_MEMORY_LIMIT=512M php occ
```


# Backup

Using the backup.sh script you easily back up the entire infrastructure. For example after mounting an external SSD to /mnt/ssd and creating a folder for the backups we can do something like this:

```bash
docker-compose stop
./backup.sh /mnt/ssd/nextcloudBackups
```

This process backs up the data contained in the Docker volumes. The containers itself don't have to be backed up. To resume the whole infrastructure:

```bash
docker-compose start
```

# Restore

When restoring the data the old volumes and containers have to be removed. The restore script will recreate the Docker volumes by unpacking the contents of the backup directory.

```bash
docker rm nextcloud_nginx nextcloud nextcloud_db
docker volume rm nextcloud_db nextcloud_nextcloud nextcloud_nginx_letsencrypt

./restore.sh /mnt/ssd/nextcloudBackup/yourBackupDirectory
docker-compose up -d
```

