#!/usr/bin/env bash

set -e

if [ ! -d "$1" ]; then
  echo "Please provide an argument to the target backup directory"
  exit 1
fi

backupDirName="backup-$(date +%d-%m-%Y)"
backupDir="$1/$backupDirName"

if [ -d "$backupDir" ]; then
  echo "$backupDir already exists"
  exit 1
fi

assert_container_state () {
  local status=$(docker inspect -f '{{.State.Status}}' $1)

  if [ "$status" == "running" ]; then
    echo "Container '$1' has to be exited."
    echo "Hint, use: 'docker-compose stop'"
    exit 1
  elif [ "$status" != "exited" ]; then
    echo "Container '$1' must exist and has to be exited"
    exit 1
  fi
}

assert_container_volume () {
  docker inspect -f '{{.Config.Volumes}}' $1 | grep $2 > /dev/null

  if [ $? -ne 0 ]; then
    echo "Expected the '$1' container to have a volume pointing internally at $2"
    exit 1
  fi
}

assert_container_state nextcloud_db
assert_container_state nextcloud

assert_container_volume nextcloud_db /var/lib/postgresql/data
assert_container_volume nextcloud /var/www/html

mkdir "$backupDir"
cd "$backupDir"

docker run --rm --volumes-from nextcloud_db -v "$(pwd)":/backup alpine sh -c "cd /var/lib/postgresql/data/ && tar cvf /backup/postgres.tar ."
docker run --rm --volumes-from nextcloud -v "$(pwd)":/backup alpine sh -c "cd /var/www/html/ && tar cfv /backup/nextcloud.tar ."
